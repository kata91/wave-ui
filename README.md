# wave-ui

GitLab project which includes basic test cases for Bynder Wave UI automation. 

## Getting started
In this file we will cover installation instructions, different ways of test execution and existing CI pipeline.

## Installation
For running the tests in local environment, we need following:
- Git (https://git-scm.com/downloads)
- VS Code or similar (https://code.visualstudio.com/download)
- Node.js installation (available on official website https://nodejs.org/en/) - project was created using Node.js 16.16.0
- Cypress installation which can be configured by running following commands (terminal should be started from project folder)
    `npm install cypress --save-dev`
- In case of running the tests with Docker for Desktop, it is needed to set it up by following install steps as per official website: https://docs.docker.com/get-docker/

## Description

Some basic test scenarios are listed in testscenarios.feature file, not all have been implemented due to time limitation but can be discussed in more details.

Code for test cases can be found cypress\e2e\login.cy.js, which uses predefined methods from cypress\pages\login_page.js. Separate file has been created for login page in order to simplify main spec, make it more concise and increase usability of the created methods.

Tests have only been executed using Google Chrome but can easily be adjusted to execute on other relevant browsers.

#### Test scenarios
As mentioned in previous section, basic test cases have been implemented, more have been listed in testscenarios.feature file but happy to discuss further scenarios in the call.
Implemented are the following:
- Navigates to login page and enters valid credentials - login successful
- Navigates to login page and enters invalid password - error on login page
- Navigates to login page and enters invalid email - error on login page
- Navigates to login page and throws an error if login is tried with empty email field
- Navigates to login page and throws an error if login is tried with empty password field

## Test Execution
There are several ways in which the tests can be run:
- Running locally using VS Code and Docker for Desktop
- By kicking off CI pipeline created in GitLab (https://gitlab.com/kata91/wave-ui/-/pipelines)

#### Running locally:
- Pull the repo locally and open it in VS Code
- In VS code terminal run following commands:

    `docker pull cypress/included:10.3.0` -> pulls official Cypress image used in Docker container

    `docker run -it --entrypoint=cypress cypress/included:10.3.0 info` -> sets an entrypoint and prints info regarding system info and available browsers

    `docker run -it -v ${PWD}:/e2e -w /e2e cypress/included:10.3.0 --spec cypress/e2e/*.js --browser chrome` -> command to run specs from cypress/e2e folder using Chrome browser

#### Running in CI
- Navigate to https://gitlab.com/kata91/wave-ui/-/pipelines and trigger pipeline manually by clicking on Run Pipeline button in the top of the page
- Run will also be triggered upon any commit

### Test Report
Test report remains to be created; currently test results can only be tracked upon running the tests in CI or locally.



