/// <reference types="cypress" />
import {LoginPage} from "../pages/login_page"

const loginPage = new LoginPage()
const login_url = 'https://wave-trial.getbynder.com/login/'
const target_url = 'https://wave-trial.getbynder.com/account/dashboard/'
const valid_password = 'testP@ssword'
const invalid_password = 'test'
const valid_email = 'lazovickatarina@proton.me'
const invalid_email = 'invalid@email'

// Test suite for checking Login page
describe('Verify basic login functionality', () => {
    // Navigates to login page and enters valid credentials
    it('allows user to login with valid credentials', function() {
        // Steps
        loginPage.navigate(login_url);
        loginPage.enterEmail(valid_email);
        loginPage.enterPassword(valid_password);
        loginPage.clickLogin();
        loginPage.currentUrl(target_url);
        loginPage.clickProfile();
        loginPage.logout();
        loginPage.currentUrl(login_url);
    })
    // Navigates to login page and enters invalid password
    it('throws an error if invalid password is entered', function() {
        // Steps
        loginPage.navigate(login_url);
        loginPage.enterEmail(valid_email);
        loginPage.enterPassword(invalid_password);
        loginPage.clickLogin();
        loginPage.invalidCredentialsValidation();    
    })

    // Navigates to login page and enters invalid email
    it('throws an error if incorrect email is entered', function() {
        // Steps
        loginPage.navigate(login_url);
        loginPage.enterEmail(invalid_email);
        loginPage.enterPassword(valid_password);
        loginPage.clickLogin();
        loginPage.invalidCredentialsValidation();    
    })

    //Navigates to login page and throws an error if login is tried with empty email field
    it('throws an error if empty email is entered', function() {
        // Steps
        loginPage.navigate(login_url);
        loginPage.enterEmail(' ');
        loginPage.enterPassword(invalid_password);
        loginPage.clickLogin();
        loginPage.invalidCredentialsValidation();    
    })

    //Navigates to login page and throws an error if login is tried with empty password
    it('throws an error if empty password is entered', function() {
        // Steps
        loginPage.navigate(login_url);
        loginPage.enterEmail(valid_email);
        loginPage.enterPassword(' ');
        loginPage.clickLogin();
        loginPage.invalidCredentialsValidation();    
    })
    
})
