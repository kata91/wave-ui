export class LoginPage{
    
    login_email_id = '#inputEmail'
    login_password_id = '#inputPassword'
    login_loginButton_id = '.login-btn'
    profile_id = '.profile'
    logoutButton_id = 'form > .action-btn'
    target_url = 'https://wave-trial.getbynder.com/account/dashboard/'
    wrong_credentials_notification_header_id = '.notification > h1'
    wrong_credentials_notification_login_id = '.cbox_messagebox'
    security_check_form_id = '.form > h1'
    

    // Method used for navigating to desired url
    navigate(url){
        cy.visit(url)
    }

    // Verifies empty string is not passed as argument and inputs email into Email/Username field
    enterEmail(email){
        expect(email, 'username was set').to.be.a('string').and.not.be.empty
        cy.get(this.login_email_id).type(email)
    }

    // Verifies empty string is not passed as argument nd inputs password into Password field
    enterPassword(password){
        expect(password, 'password was set').to.be.a('string').and.not.be.empty
        cy.get(this.login_password_id).type(password)
    }

    // Clicks Login button
    clickLogin(){
        cy.get(this.login_loginButton_id).click()
    }

    // Clicks on Profile dropdown
    clickProfile(){
        cy.get(this.profile_id).click()
        
    }

    // Clicks on Logout button
    logout(){
        cy.get(this.logoutButton_id).click()
    }

    // Retrieves current url
    currentUrl(url){
        cy.url().should('eq', url)
    }

    // Validation for invalid credentials
    invalidCredentialsValidation(){
        cy.get("body").then($body => {
            if ($body.find(this.security_check_form_id).length > 0) {
                cy.get(this.security_check_form_id).should('have.text', 'Security Check')
                    cy.log('Too many incorrect password retries. Please navigate to portal and add Captcha validation.')
            }
            else {
                cy.get(this.wrong_credentials_notification_header_id).should('be.visible');
                cy.get(this.wrong_credentials_notification_header_id).contains('You have entered an incorrect username or password.')
                cy.get(this.wrong_credentials_notification_login_id).should('be.visible');
                cy.get(this.wrong_credentials_notification_login_id).contains('You have entered an incorrect username or password.')
                cy.log('You have entered an incorrect username or password.')
            }
        })    
    }

}