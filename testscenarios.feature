Feature: Bynder Wave Login functionlity
    As a wave-ui user
    In order to use the Bynder Wave application
    I want to login with email and password

  Background:
    Given I go to '/login'
    And the field 'Email/Username' is empty
    And the field 'Password' is empty
  
  Scenario: Error on empty fields
    When I click on 'Login'
    Then I will get a message 'Please enter your password.'

  Scenario: Error on empty password
    When I type 'lazovickatarina@proton.me' in 'Email/Username'
    And I click on 'Login'
    Then I should see a message 'Please enter your password.'

  Scenario: Error on empty username
    Given field 'Email/Username' is empty
    When I type 'test' in 'Password'
    And I click on 'Login'
    Then I should see a message 'Please enter your password.'

  Scenario: Wrong password
    When I type 'lazovickatarina@proton.me' in 'Email/Username'
    And I type 'incorrect' in 'Password'
    And I click on 'Login' button
    Then I should see 'You have entered an incorrect username or password.'

  Scenario: Login successfully
    Given I have users:
      | email                      | password       |
      | lazovickatarina@proton.me  | testP@ssword   |
    When I type 'lazovickatarina@proton.me' in 'Email/Username'
    And I type 'testP@ssword' in 'password'
    And I click on 'Login'
    Then I should be logged in and see page 'https://wave-trial.getbynder.com/account/dashboard/'

  Scenario: Login is rememberd
    Given I am logged in successfully and I am located on page 'https://wave-trial.getbynder.com/account/dashboard/'
    When I click on the 'Back' arrow
    Then I will stay on the refreshed '/dashboard' page

  Scenario: Logout successfully
    Given I am logged in successfully and I am located on page 'https://wave-trial.getbynder.com/account/dashboard/'
    When I click on the profile dropdown menu
    And I click on 'Logout' button
    Then I should be redirected to page 'https://wave-trial.getbynder.com/login/'
    And I should see message 'You have successfully logged out'

  Scenario: Lost password
    Given I have forgotten my password
    When I click on 'Lost password?'
    Then I should be redirected to the page 'https://wave-trial.getbynder.com/user/forgotPassword/?redirectToken='

  Scenario: Change to Italian language   
    Given I want to change language on the login page
    When I click on Language dropdown
    And I click on 'Italiano (Italia)'
    Then I should see the login page and all the elements translated into Italian

  Scenario: Contact Support
    Given I need to contact Support
    When I click on 'Support' icon   
    Then I should see a 'Support' popup form

  Scenario: Return to Bynder homepage   
    Given I want to navigate to Bynder homepage
    When I click on Bynder icon
    Then I should be redirected to page 'https://www.bynder.com/en/'
